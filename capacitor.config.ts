import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'GR8CAST',
  webDir: 'www',
  server: {
    androidScheme: 'https'
  }
};

export default config;
