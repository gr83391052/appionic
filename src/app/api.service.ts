import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  path: string = 'http://apigr8.test/api';

  constructor(public http: HttpClient) {
    console.log('El servicio del apie sta funcionando correctamente');
  }

  get(uri: any) {
    return this.http.get(this.path + uri)
  }

  post(uri: any, data: any) {
    return this.http.post(this.path + uri, data)
  }

  update(uri: any, data: any) {
    return this.http.patch(this.path + uri, data)
  }

  delete(uri: any) {
    return this.http.delete(this.path + uri)
  }

}
