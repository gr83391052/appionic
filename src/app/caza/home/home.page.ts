import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  carouselConfig = {
    slidesToShow: 3,  // Número de elementos a mostrar al mismo tiempo
    slidesToScroll: 1,
    infinite: true,   // Carrusel infinito
    arrows: true,     // Flechas de navegación
  };
  avatars = [
    'https://ionicframework.com/docs/img/demos/avatar.svg',
    'https://ionicframework.com/docs/img/demos/avatar.svg',
    'https://ionicframework.com/docs/img/demos/avatar.svg',
    'https://ionicframework.com/docs/img/demos/avatar.svg',
    'https://ionicframework.com/docs/img/demos/avatar.svg',
    'https://ionicframework.com/docs/img/demos/avatar.svg',
    'https://ionicframework.com/docs/img/demos/avatar.svg',
    'https://ionicframework.com/docs/img/demos/avatar.svg',
    'https://ionicframework.com/docs/img/demos/avatar.svg',
    'https://ionicframework.com/docs/img/demos/avatar.svg',
    'https://ionicframework.com/docs/img/demos/avatar.svg',
    'https://ionicframework.com/docs/img/demos/avatar.svg',
    'https://ionicframework.com/docs/img/demos/avatar.svg',
    'https://ionicframework.com/docs/img/demos/avatar.svg'
  ]
  constructor() { }

  ngOnInit() {
  }

}
