import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.html',
  styleUrls: ['./registro.page.scss'],
})
export class RegistroPage implements OnInit {
  esActivo: string = 'talentos';
  name: any = ''
  typeDocument: any = ''
  document: any = ''
  phone: any = ''
  genre: any = ''
  birthday: any = ''
  country: any = ''
  city: any = ''

  constructor(
    private router: Router,
    private apiService: ApiService
  ) { }

  ngOnInit() {
  }


  seleccionarTab(tab: string): void {
    this.esActivo = tab;
  }

  navegarARegistro() {
    this.router.navigate(['/registro/caracteristicas']);
  }

  registrar() {
    const playload = {
      name: this.name,
      typeDocument: this.typeDocument,
      document: this.document,
      phone: this.phone,
      genre: this.genre,
      birthday: this.birthday,
      country: this.country,
      city: this.city,
      type: 'CAZATALENTOS'
    }

    this.apiService.post('/register/caza', playload).subscribe((result: any) => {
      if (result.status == 200) {
        const code = Math.floor(10000 + Math.random() * 90000) + ""
        localStorage.setItem('nombre', result.data.name)
        localStorage.setItem('telefono', result.data.phone)
        localStorage.setItem('code', code)
        this.router.navigate(['/codigo']);
      }
    })
  }

}
