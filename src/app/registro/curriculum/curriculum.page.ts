import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-curriculum',
  templateUrl: './curriculum.page.html',
  styleUrls: ['./curriculum.page.scss'],
})
export class CurriculumPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  navegarARegistro() {
    this.router.navigate(['/talento/home']);
  }

}
