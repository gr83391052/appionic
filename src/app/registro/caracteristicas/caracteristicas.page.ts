import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-caracteristicas',
  templateUrl: './caracteristicas.page.html',
  styleUrls: ['./caracteristicas.page.scss'],
})
export class CaracteristicasPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }


  navegarARegistro() {
    this.router.navigate(['/registro/categorias']);
  }

}
