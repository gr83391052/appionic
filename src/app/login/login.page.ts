import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  rol: string = ''
  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {

  }


  irCodigo() {
    this.route.paramMap.subscribe(params => {
      this.router.navigate(['/codigo/' + params.get('area')])
    });
  }
}
