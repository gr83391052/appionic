import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./inicio/inicio.module').then(m => m.InicioPageModule)
  },
  {
    path: 'home',
    redirectTo: 'folder/Inbox',
    pathMatch: 'full'
  },
  {
    path: 'folder/:id',
    loadChildren: () => import('./folder/folder.module').then(m => m.FolderPageModule)
  },
  {
    path: 'registro',
    loadChildren: () => import('./registro/registro.module').then(m => m.RegistroPageModule)
  },
  {
    path: 'talento/home',
    loadChildren: () => import('./talento/home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'login/:area',
    loadChildren: () => import('./login/login.module').then(m => m.LoginPageModule)
  },
  {
    path: 'codigo/',
    loadChildren: () => import('./codigo/codigo.module').then(m => m.CodigoPageModule)
  },
  {
    path: 'caza/home',
    loadChildren: () => import('./caza/home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'caza/buscador',
    loadChildren: () => import('./caza/buscador/buscador.module').then(m => m.BuscadorPageModule)
  },
  {
    path: 'caza/historial',
    loadChildren: () => import('./caza/historial/historial.module').then(m => m.HistorialPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
